# Lua Exercise

## General Instructions
- The exercise time is limited to 4 hours.
- You are allowd to take breaks and eat.
- You are not allowed to ask for assistance from the other candidates
- When you are done, send the script by email as attachment to yaniv.d@develeap.com

## The Story:
- Acme Bank is using a micro-service to log their employees’ actions in the bank’s system.
- The actions that are logged are: log-in, log-out, deposit, withdraw, phone-call.
- Each employee has a unique ID
A line in the log looks like this:  
```16:27:48 [3247] deposit```  
where 16:27:48 is the time that the action was performed.  
the part [3247] is the employee’s unique id inside [ ]  
and the word ‘deposit’ at the end of the line is the name of the action

The Exercise:  
You need to find the average work hours of an employee per day by analysing the log, using the Lua language.

Install the Lua language on the computer you use to implement the exercise.

The logs attached are from different branches:  
Log 1 - taken from a week of work in a branch that has only one employee.  
Log 2 - taken from the central branch, where 30 employees work.

For each of the logs you need to give a report of the average work time per employee.
In the report each line will be formatted as ```<employee id> - <hh>:<mm>```. For example:
```
3247 - 08:43
2581 - 09:03
6301 - 05:41
```

Note that the time should be rounded UP, e.g. if a worker works 3.98 hours, their time will be recorded as 4:00, if 3.44 then 3:27.
